package fit.cvut.cz.tjv.client.ui;

import fit.cvut.cz.tjv.client.data.OrderClient;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.web.reactive.function.client.WebClientException;

import java.time.format.DateTimeParseException;
import java.util.Date;

@ShellComponent
public class OrderConsole {

    private final OrderClient orderClient;
    private final OrderView orderView;

    public OrderConsole(OrderClient orderClient, OrderView orderView) {
        this.orderClient = orderClient;
        this.orderView = orderView;
    }

    @ShellMethod("Retrieve list of all orders")
    public void allNumbers() {
        try {
            System.out.println("All order numbers:");
            var orders = orderClient.readAll();
            orderView.printAllOrderNumbers(orders);
        } catch (WebClientException e) {
            orderView.printErrorGeneric(e);
        }
    }



    @ShellMethod("Add new order")
    public void createOrder(String orderNumber) {
        try {
            orderView.printOrder(orderClient.create(orderNumber));
        } catch (WebClientException e) {
            orderView.printErrorCreate(e);
        }
    }

    @ShellMethod("Update order number")
    public void updateOrder(String oldNumber, String newNumber) {
        try {
            orderClient.update(oldNumber, newNumber);
        } catch (WebClientException | DateTimeParseException e) {
            orderView.printErrorUpdate(e);
        }
    }

    @ShellMethod("Filter the orders starting with specified number and after the specified date")
    public void filterNumbers(String number, Date date) {
        try {
            System.out.println("All onumbers that start with:" + number + "after the date:" + date + "\n");
            var orders = orderClient.filter(number, date);
            orderView.printFilteredNumbers(orders);
        } catch (WebClientException e) {
            orderView.printErrorGeneric(e);
        }
    }

    @ShellMethod("Delete order by ID")
    public void deleteOrder(Integer id) {
        try {
            orderClient.delete(id);
        } catch (WebClientException e) {
            orderView.printErrorDelete(e);
        }
    }


}



