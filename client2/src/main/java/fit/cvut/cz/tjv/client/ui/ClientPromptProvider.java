package fit.cvut.cz.tjv.client.ui;


import fit.cvut.cz.tjv.client.data.OrderClient;
import org.jline.utils.AttributedString;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.stereotype.Component;

@Component
public class ClientPromptProvider implements PromptProvider {
    private final OrderClient orderClient;

    public ClientPromptProvider(OrderClient orderClient) {
        this.orderClient = orderClient;
    }

    @Override
    public AttributedString getPrompt() {
        return new AttributedString("Command -->");
    }
}
