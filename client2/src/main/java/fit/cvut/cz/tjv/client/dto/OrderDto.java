package fit.cvut.cz.tjv.client.dto;

import java.util.Date;

public class OrderDto {

    private Integer id;

    private String number;

    private Date date;


    public OrderDto(String number) {
        this.number = number;
    }

    public OrderDto() {
    }

    public Integer getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

}
