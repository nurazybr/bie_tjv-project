package fit.cvut.cz.tjv.client.ui;

import fit.cvut.cz.tjv.client.dto.OrderDto;
import org.springframework.boot.ansi.AnsiColor;
import org.springframework.boot.ansi.AnsiOutput;
import org.springframework.shell.ExitRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClientException;
import org.springframework.web.reactive.function.client.WebClientRequestException;
import org.springframework.web.reactive.function.client.WebClientResponseException;

import java.time.format.DateTimeParseException;
import java.util.Collection;

@Component
public class OrderView {

    public void printErrorGeneric(Throwable e) {
        if (e instanceof WebClientRequestException wcre) {
            System.err.println("Cannot connect to Social Network API. Is it accessible at `" + wcre.getUri() + "'?");
            throw new ExitRequest();
        }
        else if (e instanceof DateTimeParseException)
            System.err.println("Invalid date/time format");
        else if (e instanceof WebClientResponseException.InternalServerError)
            System.err.println("Technical server error: ");
        else
            System.err.println("Unknown error. Is Jewelry API running?");
    }

    void printErrorCreate(WebClientException e) {
        if (e instanceof WebClientResponseException.Conflict) {
            System.err.println("Order already exists.");
        } else
            printErrorGeneric(e);
    }


    public void printOrder(OrderDto order) {
        System.out.println("Order number: " + order.getNumber());
        System.out.println("    date of order: " + order.getDate());
    }


    void printAllOrderNumbers(Collection<OrderDto> orders) {
        orders.forEach(u -> System.out.print("ID-" + u.getId() + "): " + " `"  +
                AnsiOutput.toString(AnsiColor.BLUE, u.getNumber() , AnsiColor.DEFAULT) +
                "' \n" + u.getDate() + "\n"));
        System.out.println();
    }

    void printFilteredNumbers(Collection<OrderDto> orders) {
        orders.forEach(u -> System.out.print("ID-" + u.getId() + "): " + " `"  +
                AnsiOutput.toString(AnsiColor.YELLOW, u.getNumber() , AnsiColor.DEFAULT) +
                "' \n" + u.getDate() + "\n"));
        System.out.println();
    }

    public void printErrorUpdate(Throwable e) {
        if (e instanceof WebClientResponseException.NotFound)
            System.err.println(AnsiOutput.toString(AnsiColor.RED, "Cannot update: Order number does not exist!", AnsiColor.DEFAULT));
        else
            printErrorGeneric(e);
    }

    public void printErrorDelete(Throwable e) {
        if (e instanceof WebClientResponseException.NotFound)
            System.err.println(AnsiOutput.toString(AnsiColor.RED, "Cannot delete: Order number doesnt exist!", AnsiColor.DEFAULT));
        else
            printErrorGeneric(e);
    }




}
