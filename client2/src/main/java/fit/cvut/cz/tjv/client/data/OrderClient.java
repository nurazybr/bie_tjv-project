package fit.cvut.cz.tjv.client.data;

import fit.cvut.cz.tjv.client.dto.OrderDto;
import fit.cvut.cz.tjv.client.ui.OrderView;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;


import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.Collection;
import java.util.Date;

@Component
public class OrderClient {
    private static final String ONE_URI = "/{id}";
    private final WebClient orderWebClient;
    private final OrderView orderView;
    SimpleDateFormat sm = new SimpleDateFormat("yyyy/MM/dd");

    public OrderClient(@Value("${jewelry_client_url}") String backendUrl, OrderView orderView) {
        orderWebClient = WebClient.create(backendUrl + "/orders");
        this.orderView = orderView;
    }



    public OrderDto create(String order) {
        return orderWebClient.post()// HTTP POST
                .uri("/add")
                .contentType(MediaType.APPLICATION_JSON) // set HTTP header
                .bodyValue(order) // POST data
                .retrieve() // request specification finished
                .bodyToMono(OrderDto.class) // interpret response body as one element
                .block(Duration.ofSeconds(5)); // call synchronously with timeout
    }


    public Collection<OrderDto> readAll() {
        return orderWebClient.get() // HTTP GET
                .uri("/all")
                .retrieve() // request specification finished
                .bodyToFlux(OrderDto.class) // interpret response body as a collection
                .collectList() // collect all elements as list
                .block(Duration.ofSeconds(5)); // call synchronously with timeout
    }

    public Collection<OrderDto> filter(String number, Date date) {
        String strDate = sm.format(date);
        return orderWebClient.get() // HTTP GET
                .uri(uriBuilder -> uriBuilder               // URI build for a query
                        .path("/filter")
                        .queryParam("orderNumber", "{number}")
                        .queryParam("afterDate", "{date}")
                        .build(number, strDate))
                .retrieve() // request specification finished
                .bodyToFlux(OrderDto.class) // interpret response body as a collection
                .collectList() // collect all elements as list
                .block(Duration.ofSeconds(5)); // call synchronously with timeout
    }

    public void update(String oldNumber, String newNumber) {
        orderWebClient.put() // HTTP PUT
                .uri("/updateOrderNumber" + ONE_URI, oldNumber) // URI /{id}
                .contentType(MediaType.APPLICATION_JSON) // HTTP header
                .bodyValue(newNumber) // HTTP body
                .retrieve() // request specification finished
                .toBodilessEntity() // do not extract HTTP response body
                .subscribe(x -> {
                }, e -> {
                    orderView.printErrorUpdate(e);
                }) // register callbacks: for success and for error
        ;
    }

    public void delete(Integer id) {
        orderWebClient.delete() // HTTP DELETE
                .uri(ONE_URI, id) // URI
                .retrieve() // request specification finished
                .toBodilessEntity() // do not extract HTTP response body
                .subscribe(x -> {}, e -> {
                    orderView.printErrorDelete(e);
                }) // register callbacks: for success and for error
        ;
    }

}
