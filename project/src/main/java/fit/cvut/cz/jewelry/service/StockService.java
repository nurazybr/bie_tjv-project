package fit.cvut.cz.jewelry.service;

import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class StockService {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private OrderService orderService;

    public Stock createStock(String type, String dct, String ...orderNumbers) {
        if (orderNumbers.length == 0) throw new RuntimeException("Jewelry must be assigned to a order");

        Set<Order> orderSet = getOrders(orderNumbers);

        final Stock stock = stockRepository.save(new Stock(type, dct, orderSet));

        orderSet.stream().filter(order -> order.getNumber() != null).forEach(order -> orderService.addJewelry(order.getNumber(), stock));
        return stock;
    }

    private Set<Order> getOrders(String... orderNumbers) {
        List<String> orderNumberList = Arrays.asList(orderNumbers);

        return orderNumberList.stream().map(orderNumber ->
                orderService.findByNumber(orderNumber)).collect(Collectors.toSet());
    }

    public List<Stock> listAllStock() {
        return stockRepository.findAll();
    }

    public void deleteById(Integer id) {
        stockRepository.deleteById(id);
    }

    public Stock findByDct(String dct) {
        return stockRepository.findByDct(dct).orElseThrow(() -> new RuntimeException("Stock not found by DCT"));
    }

    public Stock updateStock(Integer id, String type, String dct) {
        Stock stock2 = stockRepository.findById(id).map(stock1 -> {
            stock1.setDct(dct);
            stock1.setType(type);
            return stock1;
        }).orElseThrow(() -> new RuntimeException("Stock failed to update"));
        Stock stockSaved = stockRepository.save(stock2);
        return stockSaved;
    }
    }