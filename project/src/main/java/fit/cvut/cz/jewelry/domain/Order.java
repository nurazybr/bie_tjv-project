package fit.cvut.cz.jewelry.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Table(name = "T_ORDER")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "DATE")
    private Date date;


    @Column (name = "NUMBER")
    private String number;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "T_STOCK_ORDER",
            joinColumns = { @JoinColumn(name = "ORDER_ID") },
            inverseJoinColumns = { @JoinColumn(name = "STOCK_ID") }
    )
    private Set<Stock> jewelry;

    public Order ( String number) {
        this.number = number;
        this.jewelry = new HashSet<>();
    }

    public Order() {
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }

    public Set<Stock> getJewelry() {
        return jewelry;
    }
    public void setJewelry(Set<Stock> jewelry) {
        this.jewelry = jewelry;
    }
}
