package fit.cvut.cz.jewelry.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "T_STOCK")
public class Stock {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "TYPE")
    private String type;

    @Column (name = "DCT")
    private String dct;

    @ManyToMany ( mappedBy = "jewelry")
    transient private Set<Order> order;

    public Stock( String type, String dct, Set<Order> order) {
        this.type = type;
        this.dct = dct;
        this.order = order;
    }

    public Stock() {

    }

    //


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }

    //
    public String getDct() {
        return dct;
    }
    public void setDct(String dct) {
        this.dct = dct;
    }

    public Set<Order> getOrder() {
        return order;
    }
    public void setOrder(Set<Order> order) {
        this.order = order;
    }
}
