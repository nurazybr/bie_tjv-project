package fit.cvut.cz.jewelry.controller;

import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.service.StockService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/stock")
public class StockController {


    @Autowired
    private StockService stockService;

    @PostMapping(path = "/create")
    public Stock createStock(@RequestBody StockForm form) {
        return stockService.createStock(form.getType(), form.getDct(), form.getOrderNumbers());
    }

    @GetMapping(path = "/all")
    public List<Stock> findAll() {
        return stockService.listAllStock();
    }

    @GetMapping(path = "/findbydct")
    public Stock findByDct(@RequestBody String dct) {
        return stockService.findByDct(dct);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        stockService.deleteById(id);
    }

    @PutMapping(path = "/updateStock/{id}")
    public Stock changeStock(@RequestBody StockForm form, @PathVariable Integer id) {
        return stockService.updateStock(id, form.getType(), form.getDct());
    }


    @Data
    private static class StockForm {
        private String type;
        private String dct;
        private String[] orderNumbers;

    }

}
