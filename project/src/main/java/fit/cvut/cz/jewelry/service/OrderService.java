package fit.cvut.cz.jewelry.service;

import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;


@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerService customerService;



    public Order createOrder(String number) {
        Order order = new Order(number);
        order.setNumber(number);
        order.setDate(new Date());

        return save(order);
    }

    public Order save(Order order) {
        return orderRepository.save(order);
    }

    public Order findByNumber(String number) {
        return orderRepository.findByNumber(number).orElseThrow(() -> new RuntimeException("Order not found : " + number));
    }

    public Order addJewelry(String orderNumber, Stock stock) {
        Order order = findByNumber(orderNumber);

        if (order.getJewelry() != null) {
            order.getJewelry().add(stock);
        }

        return orderRepository.save(order);
    }

    public Customer setCustomer(String orderNumber, Customer customer) {
        Order order = findByNumber(orderNumber);

       customer.setOrder(order);

        return customerService.save(customer);
    }

    public void deleteById(Integer id) {
        orderRepository.deleteById(id);
    }


    public Order updateOrderNumber(String oldNumber, String orderNumber) {
        Order order2 = orderRepository.findByNumber(oldNumber).map(order1 -> {
            order1.setNumber(orderNumber);
            return order1;
        }).orElseThrow(() -> new RuntimeException("Order number failed to update"));
        Order orderSaved = orderRepository.save(order2);
        return orderSaved;
    }



    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    public List<Order> filter(String orderNumber, Date afterDate) {
        return orderRepository.filter(orderNumber, afterDate);
    }

}
