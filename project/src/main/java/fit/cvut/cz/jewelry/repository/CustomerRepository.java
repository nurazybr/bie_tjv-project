package fit.cvut.cz.jewelry.repository;

import fit.cvut.cz.jewelry.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CustomerRepository extends JpaRepository <Customer, Integer> {

    Optional<Customer> findByOrderNumber(String number);

}
