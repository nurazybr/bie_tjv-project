package fit.cvut.cz.jewelry.repository;

import fit.cvut.cz.jewelry.domain.Stock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StockRepository extends JpaRepository<Stock, Integer> {
    Optional<Stock> findByDct(String dct);
}
