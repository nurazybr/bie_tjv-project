package fit.cvut.cz.jewelry.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "T_CUSTOMER")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column (name = "NAME")
    private String name;

    @Column (name = "SURNAME")
    private String surname;

    @Column (name = "ADDRESS")
    private String address;

    @Column (name = "EMAIL")
    private String email;

    @OneToOne(fetch = FetchType.EAGER)
    @MapsId
    private Order order;


    public Customer(String name, String surname, String address, String email, Order order) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.email = email;
        this.order = order;
    }

    public Customer() {
    }


    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    //

    public Order getOrder() {
        return order;
    }
    public void setOrder(Order order) {
        this.order = order;
    }
}
