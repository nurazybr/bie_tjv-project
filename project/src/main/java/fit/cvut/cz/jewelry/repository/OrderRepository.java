package fit.cvut.cz.jewelry.repository;

import fit.cvut.cz.jewelry.domain.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Integer> {
    
    Optional<Order> findByNumber(String number);

    @Query("from Order o where o.number like :orderNumber% and o.date >= :afterDate")
    List<Order> filter(@PathVariable String orderNumber, @PathVariable Date afterDate);

}
