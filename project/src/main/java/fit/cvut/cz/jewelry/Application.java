package fit.cvut.cz.jewelry;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@ComponentScan ({"fit.cvut.cz.jewelry.controller","fit.cvut.cz.jewelry.service","fit.cvut.cz.jewelry.domain","fit.cvut.cz.jewelry.repository"})
@EnableJpaRepositories("fit.cvut.cz.jewelry.repository")
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}


//ALTER TABLE T_ORDER ADD CONSTRAINT order_pk PRIMARY KEY ( ID );