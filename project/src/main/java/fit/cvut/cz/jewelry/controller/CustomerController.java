package fit.cvut.cz.jewelry.controller;


import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/all")
    public List<Customer> findAll() {
        return customerService.findAll();
    }

    @GetMapping("/findByOrder")
    private Customer findByOrderNumber(@RequestParam String orderNumber) {
        return customerService.findByOrderNumber(orderNumber);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        customerService.deleteById(id);
    }
}
