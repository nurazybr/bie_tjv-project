package fit.cvut.cz.jewelry.service;

import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private OrderService orderService;

    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    public Customer findById(Integer id) {
        return customerRepository.findById(id).orElseThrow(()-> new RuntimeException("Customer not found"));
    }

    public void deleteById(Integer id) {
        customerRepository.deleteById(id);
    }

    public Customer findByOrderNumber(String number) {
        return customerRepository.findByOrderNumber(number).orElseThrow(()-> new RuntimeException("Customer not found"));
    }


    public Customer updateCustomer(Integer customerId, String orderNumber) {
        Customer customer = findById(customerId);
        Order order = orderService.findByNumber(orderNumber);
        customer.setOrder(order);
        return customerRepository.save(customer);

    }
}
