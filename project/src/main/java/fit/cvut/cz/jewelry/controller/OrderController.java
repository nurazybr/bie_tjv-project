package fit.cvut.cz.jewelry.controller;

import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.service.CustomerService;
import fit.cvut.cz.jewelry.service.OrderService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;


@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CustomerService customerService;

    @PostMapping(path = "/add")
    public Order addOrder(@RequestBody String number) {
        return orderService.createOrder(number);
    }

    @PostMapping(path = "/addJewelry")
    public Order addJewelry(@RequestBody AddJewelryForm form) {
        return orderService.addJewelry(form.getOrderNumber(), form.getStock());
    }

    @PostMapping(path = "/setCustomer")
    public Customer setCustomer(@RequestBody SetCustomerForm form) {
        return orderService.setCustomer(form.getOrderNumber(), form.getCustomer());
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Integer id) {
        orderService.deleteById(id);
    }

    @GetMapping(path = "/all")
    public List<Order> all() {
        return orderService.findAll();
    }

    @PutMapping("/updateCustomer/{orderNumber}")
    public Customer changeCustomer(@RequestParam Integer customerId, @PathVariable String orderNumber) {
        return customerService.updateCustomer(customerId, orderNumber);
    }

    @PutMapping(path = "/updateOrderNumber/{oldNumber}")
    public Order changeOrderNumber(@RequestBody String newNumber, @PathVariable String oldNumber) {
        return orderService.updateOrderNumber(oldNumber, newNumber);
    }

    @GetMapping(path = "/filter")
    public List<Order> filterOrder(@RequestParam String orderNumber, @RequestParam Date afterDate) {
        return orderService.filter(orderNumber, afterDate);
    }


    @Data
    private static class SetCustomerForm {
        private String orderNumber;
        private Customer customer;
    }

    @Data
    private static class AddJewelryForm {
        private String orderNumber;
        private Stock stock;

    }
}
