import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.repository.CustomerRepository;
import fit.cvut.cz.jewelry.service.CustomerService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceUnitTest {

    @Mock
    private CustomerRepository customerRepository;

    @Spy
    @InjectMocks
    private CustomerService customerService;

    @Test
    public void testFindAll() {
        List<Customer> customers = new ArrayList<>();
        doReturn(customers).when(customerRepository).findAll();
        List<Customer> result = customerService.findAll();
        assertEquals(customers, result);
    }

    @Test
    public void testSave() {
        Customer customer = new Customer();
        doReturn(customer).when(customerRepository).save(customer);
        Customer result = customerService.save(customer);
        assertEquals(customer, result);
    }

    @Test
    public void testFindById() {
        Customer customer = new Customer();
        doReturn(Optional.of(customer)).when(customerRepository).findById(any());
        Customer result = customerService.findById(any());
        assertEquals(customer, result);
    }

    @Test(expected = RuntimeException.class)
    public void testFindByNumberNotFound() {
        doReturn(Optional.empty()).when(customerRepository).findById(any());
        customerService.findById(any());
    }

    @Test
    public void testDelete() {
        customerService.deleteById(any());
        verify(customerRepository).deleteById(any());

    }

    @Test
    public void testFindByOrderNumber() {
        Customer customer = new Customer();
        String number = "a";
        doReturn(Optional.of(customer)).when(customerRepository).findByOrderNumber(number);
        Customer result = customerService.findByOrderNumber(number);
        assertEquals(customer, result);
    }

}
