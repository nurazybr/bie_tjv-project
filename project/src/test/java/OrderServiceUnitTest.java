import fit.cvut.cz.jewelry.domain.Customer;
import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.repository.CustomerRepository;
import fit.cvut.cz.jewelry.repository.OrderRepository;
import fit.cvut.cz.jewelry.service.CustomerService;
import fit.cvut.cz.jewelry.service.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceUnitTest {

    @Spy
    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private CustomerRepository customerRepository;

    @Spy
    @InjectMocks
    private CustomerService customerService;

    @Test
    public void testFindByNumber() {
        Order order = new Order();
        String number = "a";
        doReturn(Optional.of(order)).when(orderRepository).findByNumber(number);
        Order result = orderService.findByNumber(number);
        assertEquals(order, result);
    }

    @Test(expected = RuntimeException.class)
    public void testFindByNumberNotFound() {
        String number = "a";
        doReturn(Optional.empty()).when(orderRepository).findByNumber(number);
        orderService.findByNumber(number);
    }

    @Test
    public void testCreateOrder() {
        Order order = new Order();
        String number = "a";
        order.setNumber(number);
        order.setDate(new Date());
        doReturn(order).when(orderRepository).save(order);
        Order result = orderService.save(order);
        assertEquals(order, result);
    }

    @Test
    public void testAddJewelry() {
        Order order = new Order();
        Stock stock = new Stock();
        String number = "a";
        doReturn(Optional.of(order)).when(orderRepository).findByNumber(number);
        order.setJewelry(new HashSet<>());
        order.getJewelry().add(stock);
        doReturn(order).when(orderRepository).save(order);
        Order result = orderService.addJewelry(number, stock);
        assertEquals(order, result);
    }

    @Test
    public void testSetCustomer() {
        Order order = new Order();
        Customer customer = new Customer();
        customer.setOrder(order);
        doReturn(customer).when(customerRepository).save(customer);
        Customer result = customerService.save(customer);
        assertEquals(customer, result);
    }

    @Test
    public void testFindAll() {
        List<Order> orders = new ArrayList<>();
        doReturn(orders).when(orderRepository).findAll();
        List<Order> result = orderService.findAll();
        assertEquals(orders, result);
    }

    @Test
    public void testDelete() {
        orderService.deleteById(any());
        verify(orderRepository).deleteById(any());

    }

    @Test
    public void testFilter() {
        List<Order> orders = new ArrayList<>();
        String number = "A";
        Date date = new Date();
        doReturn(orders).when(orderRepository).filter(number, date);
        List<Order> result = orderService.filter(number, date);
        assertEquals(orders, result);
    }


}
