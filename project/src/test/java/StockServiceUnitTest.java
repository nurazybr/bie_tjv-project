import fit.cvut.cz.jewelry.domain.Order;
import fit.cvut.cz.jewelry.domain.Stock;
import fit.cvut.cz.jewelry.repository.OrderRepository;
import fit.cvut.cz.jewelry.repository.StockRepository;
import fit.cvut.cz.jewelry.service.OrderService;
import fit.cvut.cz.jewelry.service.StockService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class StockServiceUnitTest {

    @Spy
    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private StockRepository stockRepository;

    @Spy
    @InjectMocks
    private StockService stockService;

    @Before
    public void createMocks() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll() {
        List<Stock> stocks = new ArrayList<>();
        doReturn(stocks).when(stockRepository).findAll();
        List<Stock> result = stockService.listAllStock();
        assertEquals(stocks, result);
    }

    @Test
    public void testDelete() {
        stockService.deleteById(any());
        verify(stockRepository).deleteById(any());
    }

    @Test
    public void testFindByDCT() {
        Stock stock = new Stock();
        String dctName = "a";
        doReturn(Optional.of(stock)).when(stockRepository).findByDct(dctName);
        Stock result = stockService.findByDct(dctName);
        assertEquals(stock, result);
    }

    @Test(expected = RuntimeException.class)
    public void testFindByDCTNotFound() {
        String dctName = "a";
        doReturn(Optional.empty()).when(stockRepository).findByDct(dctName);
        stockService.findByDct(dctName);
    }

    @Test
    public void testCreateStock() {
        String type = "ABC";
        String dct = "ABCDct";
        String[] orderNumbers = {"a"};
        Order order = new Order();
        doReturn(Optional.of(order)).when(orderRepository).findByNumber("a");

        Stock stock = new Stock(type, dct, Collections.singleton(order));
        doReturn(stock).when(stockRepository).save(stock);

        Stock result = stockService.createStock(type, dct, orderNumbers);
        assertEquals(stock, result);
    }

    @Test
    public void testUpdateStock() {
        String type = "ABC";
        String dct = "ABCDct";
        Order order = new Order();

        Stock stock = new Stock(type, dct, Collections.singleton(order));
        doReturn(Optional.of(stock)).when(stockRepository).findById(1);

        doReturn(stock).when(stockRepository).save(stock);
        Stock result = stockService.updateStock(1, type, dct);
        assertEquals(stock, result);
    }

}
