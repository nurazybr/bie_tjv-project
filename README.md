TJV PROJECT 

Jewelry shop management system. 
The program takes an order, assigns customer to it with his name, address and e-mail.
assigns jewelry to an order with the type and dct(custom code for each model)
The purpose is that its easier for managers to control and overview the orders. 

Program works on Maven/Gradle and MySql database system.

To make the server work:
* Open the project in IDE and clean install the maven and buld the project, **mvn clean install** in terminal,
* **Set up the MySQL database host to the "localhost" 
  and name the database "jewelry" and the port "3306", 
  login = root
  password = hellotjv
  or its possible to change the connection to your own preference in "application properties" in "resources"** 
* run the SQL script ( in the resources) to create the tables.
* Start the server by running the **Application Class**.


By simple HTTP requests you can operate over entities order, customer, jewelry.
Examples of requests can be found in "requests.http" file, or by using SWAGGER API Documentation tool 
by opening this url in the browser: 
**http://localhost:2218/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config#/**. 

To use the client: 
* run the backend server with database system (see above). 
* open the client2 in IDE.
* open the terminal and change the directory (cd) into the working directory (client2)- **cd client2**.
* then clean build gradle: type into terminal-  **./gradlew clean build**.
* Run the application java -jar commmand-  **java -jar build/libs/client2-1.0-SNAPSHOT.jar**.
* After it started type into terminal- **help** to see available commands.



Created by Ybrahmsultan Nurazkhan, 2022. 